package com.example.training3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;

public class PhoneActivity extends AppCompatActivity {

    EditText phNam;
    EditText phNum;
    Button phAdd;
    Button phSave;

    public static SharedPreferences sp;

    public static final String USER_PREF = "USER_PREF" ;
    public static final String KEY_NAME = "KEY_NAME";
    public static final String KEY_PHONE = "KEY_PHONE";

    ArrayList<ContactItem> contactItems = new ArrayList<>();
    private RecyclerView recyclerView;
    private ContactAdapter contactAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);


        phNam = findViewById(R.id.phNam);
        phNum = findViewById(R.id.phNum);
        phAdd = findViewById(R.id.phAdd);
        phSave = findViewById(R.id.phSave);


        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        contactAdapter = new ContactAdapter(contactItems);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(contactAdapter);


        sp = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);

        phAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save(view);

            }
        });

        load();

        phSave = findViewById(R.id.phSave);
        phSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertItem(phNam.getText().toString(), Integer.parseInt(phNum.getText().toString()));
            }
        });

    }

    public void save(View v) {
        try{
        String name  = phNam.getText().toString();
        int phone  = Integer.parseInt(phNum.getText().toString());

        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_NAME, name);
        editor.putInt(KEY_PHONE, phone);
        editor.commit();

        Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show();
        } catch(NumberFormatException ex){
            Toast.makeText(getApplication(), "Can't be blank", Toast.LENGTH_SHORT).show();
        }
    }

    private void saveContact() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(contactItems);
        editor.putString("task list", json);
        editor.apply();

    }

    private void load(){
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("task list", null);
        Type type = new TypeToken<ArrayList<ContactItem>>() {}.getType();
        contactItems.addAll((Collection<? extends ContactItem>) gson.fromJson(json, type));
        contactAdapter.notifyDataSetChanged();

    }

    private void insertItem(String name, int phone) {
        contactItems.add(new ContactItem(name, phone));
        contactAdapter.notifyDataSetChanged();
        Log.e("INSERT", name);
        Log.e("INSERT", String.valueOf(contactItems.size()));

        saveContact();
    }

}