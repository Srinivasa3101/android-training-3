package com.example.training3;

public class ContactItem {
    private String name;
    private int phone;

    public  ContactItem(String names, int phones){
        name = names;
        phone = phones;
    }

    public  String getName(){
        return name;
    }

    public int getPhone(){
        return phone;
    }
}
