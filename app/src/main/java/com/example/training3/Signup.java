package com.example.training3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

public class Signup extends AppCompatActivity {

    TextInputLayout sigNam;
    TextInputLayout sigEma;
    TextInputLayout sigPws;
    Button sigBt;
    TextView sigTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        sigNam = (TextInputLayout)findViewById(R.id.sigNam);
        sigEma = (TextInputLayout)findViewById(R.id.sigEma);
        sigPws = (TextInputLayout)findViewById(R.id.sigPws);

        sigBt = (Button)findViewById(R.id.sigBt);
        sigBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplication(), MainActivity.class);
                startActivity(intent);
            }
        });

        sigTxt = (TextView)findViewById(R.id.sigTxt);
        sigTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplication(), MainActivity.class);
                startActivity(intent);
            }
        });

    }
}