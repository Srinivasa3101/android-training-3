package com.example.training3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {

    TextInputLayout logEma;
    TextInputLayout logPws;
    Button logBt;
    TextView logTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logEma = (TextInputLayout)findViewById(R.id.logEma);
        logPws = (TextInputLayout)findViewById(R.id.logPws);

        logBt = (Button)findViewById(R.id.logBt);
        logBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplication(), PhoneActivity.class);
                startActivity(intent);
            }
        });

        logTxt = (TextView)findViewById(R.id.logTxt);
        logTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplication(), Signup.class);
                startActivity(intent);
            }
        });

    }
}