package com.example.training3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {
    private ArrayList<ContactItem> contactItem;

    public static class ContactViewHolder extends  RecyclerView.ViewHolder{
        public TextView textView;
        public TextView textView2;

        public ContactViewHolder(View itemView){
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
            textView2 = itemView.findViewById(R.id.textView2);
        }
    }

    public ContactAdapter(ArrayList<ContactItem> contactl){
        contactItem = contactl;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from((parent.getContext())).inflate(R.layout.contact_item, parent, false);
        ContactViewHolder vh = new ContactViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {

        ContactItem currentItem = contactItem.get(position);
        holder.textView.setText(currentItem.getName());
        holder.textView2.setText(String.valueOf(currentItem.getPhone()));

    }

    @Override
    public int getItemCount() {
        return contactItem.size();
    }
}
